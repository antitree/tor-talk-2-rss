FROM python:3

WORKDIR /usr/src/app/

ADD mailman-rss .

RUN pip install wheel
RUN python setup.py sdist bdist_wheel; \
pip install dist/mailman*.whl

# Hack so that it forces the rebuilt to start here every time
ARG DATE
ARG LIST
RUN echo $DATE

RUN echo ${LIST}

WORKDIR /usr/src/app

RUN /bin/bash -c 'mailman-rss -l debug --command rss -o ${LIST}.rss --archive-url https://lists.torproject.org/pipermail/${LIST}'


FROM python:2

ARG LIST

RUN pip install gsutil

WORKDIR /usr/src/app

COPY --from=0 /usr/src/app/${LIST}.rss .

COPY . .

ARG GCS

RUN ["./deploy.sh"]
